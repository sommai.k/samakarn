import { FC } from "react";
import { Link } from "react-router-dom";

const MenuPage: FC = () => {
  return (
    <>
      <div className="grid grid-cols-1 gap-2 p-2 md:grid-cols-2 xl:grid-cols-3">
        <Link to="/admin/news" className="npru-button text-center">
          News
        </Link>
        <Link to="/admin/user" className="npru-button text-center">
          User
        </Link>
        <Link className="npru-button text-center" to="/admin/member-list">
          Member
        </Link>
        <button className="npru-button text-center">Menu #4</button>
        <button className="npru-button text-center">Menu #5</button>
        <button className="npru-button text-center">Logout</button>
      </div>
    </>
  );
};

export { MenuPage };
